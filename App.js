import React from 'react';
import AppContainer from './src/App';
import { Root } from "native-base";
import { ActivityIndicator } from 'react-native';
import * as Font from 'expo-font';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Muli: require("native-base/Fonts/Roboto.ttf"),
      "Muli-Bold": require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
    });
    this.setState({ loading: false });
  }

  render() {
    if (this.state.loading) {
      return (
        <Root>
          <ActivityIndicator style={{alignSelf: 'center', flex: 1}} size="large" color="#00ff00" />
        </Root>
      );
    }
    return (
      <AppContainer/>
    );
  }
}
  