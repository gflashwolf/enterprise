import { TextStyle, PixelRatio } from 'react-native';

// SOLID COLORS
export const colorDark = '#292044';
export const colorVine = '#612F6D';
export const colorVineOpacity = 'rgba(97,47,109,0.5)';
export const colorRuby = '#B14A6B';
export const colorPeach = '#E1936A';
export const colorSun = '#EFE687';
export const colorWhite = '#FFFFFF';
export const colorSmoke = '#F5F5F5';
export const colorCloudy = '#E7E7E7';
export const colorBg = '#E5E5E5';
export const colorBlack = '#000000';
export const colorDanger = '#BB2124';
export const colorGray = '#8a8a8a';
export const colorPurple = '#6e3d76';

let buttonSmallFont = 11;
let buttonSmallLineHeight = 14;
let text12Font = 12;
let text12LineHeight = 15;
let text14Font = 14;
let text14LineHeight = 18;
let text16Font = 16;
let text16LineHeight = 20;
let boxStoreFont = 20;
let boxStoreLineHeight = 25;
let titleSmallFont = 26;
let titleSmallLineHeight = 30;
let titleNormalFont = 36;
let titleNormalLineHeight = 40;

if (PixelRatio.get() < 2) {
  buttonSmallFont = 9;
  buttonSmallLineHeight = 12;
  text12Font = 10;
  text12LineHeight = 13;
  text14Font = 12;
  text14LineHeight = 16;
  text16Font = 14;
  text16LineHeight = 18;
  boxStoreFont = 18;
  boxStoreLineHeight = 23;
  titleSmallFont = 22;
  titleSmallLineHeight = 26;
  titleNormalFont = 32;
  titleNormalLineHeight = 36;
}

// Typography
export const title: TextStyle = {
  fontFamily: 'Ubuntu' as 'Ubuntu',
  display: 'flex' as 'flex',
  alignItems: 'center' as 'center',
};
export const titleSmall: TextStyle = {
  ...title,
  fontSize: titleSmallFont,
  lineHeight: titleSmallLineHeight,
};
export const titleNormal: TextStyle = {
  ...title,
  fontSize: titleNormalFont,
  lineHeight: titleNormalLineHeight,
};
export const boxStore: TextStyle = {
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
  fontSize: boxStoreFont,
  lineHeight: boxStoreLineHeight,
};
export const text: TextStyle = {
  fontFamily: 'Muli' as 'Muli',
};
export const text12: TextStyle = {
  ...text,
  fontSize: text12Font,
  lineHeight: text12LineHeight,
};
export const text12b: TextStyle = {
  ...text12,
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
};
export const text14: TextStyle = {
  ...text,
  fontSize: text14Font,
  lineHeight: text14LineHeight,
};
export const text14sb: TextStyle = {
  ...text14,
  fontFamily: 'Muli-SemiBold' as 'Muli-SemiBold',
};
export const text14b: TextStyle = {
  ...text14,
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
};
export const text16b: TextStyle = {
  ...text,
  fontSize: text16Font,
  lineHeight: text16LineHeight,
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
};

export const text16: TextStyle = {
  ...text16b,
  fontFamily: 'Muli' as 'Muli',
};
export const sessionTitle: TextStyle = {
  ...text,
  fontSize: text12Font,
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
  lineHeight: text12LineHeight,
  textTransform: 'uppercase',
};
export const buttonSmall: TextStyle = {
  ...text,
  fontSize: buttonSmallFont,
  fontFamily: 'Muli-Bold' as 'Muli-Bold',
  lineHeight: buttonSmallLineHeight,
};
export const buttonNormal: TextStyle = {
  ...text12b,
};
