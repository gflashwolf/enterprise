import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { AppState } from '../../store';
import Loading from '../../components/Loading';
import styles from './styles';

interface Props {
  userLoading: boolean;
  companiesLoading: boolean;
}

class LoadingContainer extends PureComponent<Props> {
  render() {
    const visibility = this.props.userLoading || this.props.companiesLoading;
    return (
      <Fragment>
        {
          visibility
          && (
            <View style={styles.container}>
              <Loading
                visibility={visibility}
              />
            </View>
          )
        }
      </Fragment>
    );
  }
}

const mapStateToProps = (state: AppState) => ({
  userLoading: state.user.loading,
  companiesLoading: state.companies.loading,
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(LoadingContainer);
