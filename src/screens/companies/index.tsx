import React, { Component } from "react";
import { Text, Card, CardItem, Thumbnail, Body, Container, Left, View, Right } from "native-base";
import { styles } from "../home/style";
import { Company } from '../../state/company/types';
import { NavigationScreenProp } from 'react-navigation';
import Header from '../../components/Header';
import { baseURL } from '../../../src/state/utils/api';

interface StateProps {
  navigation: NavigationScreenProp<any, any>;
}

interface State {
  enterprise: Company;
}

class Companies extends Component <StateProps, State> {
  constructor(props: StateProps) {
    super(props);
    this.state = {
      enterprise: {
        id: 0,
        enterprise_name: '',
        photo: '',
        share_price: 0,
        city: '',
        country: '',
        description: '',
        enterprise_type: {
          enterprise_type_name: '',
        }
      },
    };
  }

  componentWillMount() {
    if(this.props.navigation.state.params.enterprise) {
      const enterprise = this.props.navigation.state.params.enterprise;
      this.setState({ enterprise })
    }
  };

  render() {
    const { enterprise } = this.state;
    return (
      <Container>
         {Header(this.props)}
        <View style={styles.Content}>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail  source={{uri:`${baseURL}${enterprise.photo}`}} />
                <Body>
                  <Text>{enterprise.enterprise_name}</Text>
                  <Text note>$ {enterprise.share_price}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={[styles.text,styles.margin]}>{enterprise.enterprise_type.enterprise_type_name}</Text>
                <Text style={styles.textSub}>{enterprise.city} - {enterprise.country}</Text>
              </Body>
            </CardItem>  
            <CardItem bordered>
              <Body>
                <Text>
                  {enterprise.description}
                </Text>
              </Body>
            </CardItem>
            <CardItem>
              <Left/>
              <Body/>
              <Right/>
            </CardItem>
          </Card>
        </View>
    </Container>
    );
  }
}

export default Companies;
