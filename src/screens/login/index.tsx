import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';
import { AppState } from '../../store';
import { NavigationScreenProp } from 'react-navigation';
import * as UserActions from '../../state/user/actions';
import { styles } from './style';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert
} from 'react-native';

interface StateProps {
  navigation: NavigationScreenProp<any, any>;
}

interface DispatchProps {
  signIn(email: string, password: string): void;
}

interface State {
  email: string;
  password: string;
}

type Props = StateProps & DispatchProps;

class Login extends Component <Props, State> {
  state = {
    email: 'testeapple@ioasys.com.br',
    password: '12341234',
  }

  onLogin = (email: string, password: string) => {
    const { signIn } = this.props;
    if(email === "" || password.length < 6){
      Alert.alert("Os campos não podem ser vazios.")
      return;
    }
    signIn(email, password);
  }

  render(){
    const { email, password } = this.state;

    return ( 
        <View style={styles.container}>
          <Image source={require("../../../assets/icon-200pink.png")}/>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Email"
                keyboardType="email-address"
                autoCapitalize="none"
                underlineColorAndroid='transparent'
                value={email}
                onChangeText={(email) => this.setState({email})}/>
          </View>
          
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Password"
                secureTextEntry={true}
                value={password}
                underlineColorAndroid='transparent'
                onChangeText={(password) => this.setState({password})}/>
          </View>

          <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onLogin(email,password)}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>

          <TouchableHighlight style={styles.buttonContainer} onPress={() =>console.log('restore_password')}>
              <Text style={styles.Text}>Forgot your password?</Text>
          </TouchableHighlight>

          <TouchableHighlight style={styles.buttonContainer} onPress={() => console.log('register')}>
              <Text style={styles.Text}>Register</Text>
          </TouchableHighlight>
        </View>
    )
  }
}

const mapStateToProps = (state: AppState) => ({
  userLoading: state.user.loading,
  companyLoading: state.companies.loading,
});

const mapDispatchToProps = (dispatch: Dispatch) => (bindActionCreators(UserActions, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Login);
