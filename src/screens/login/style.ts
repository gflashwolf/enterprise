import {
  StyleSheet, ViewStyle, FlexStyle, ImageStyle, TextStyle, Dimensions, Platform,
 } from 'react-native';

interface Styles {
  Text: TextStyle;
  inputs: TextStyle;
  loginText: TextStyle;
  inputIcon: ViewStyle;
  inputContainer: ViewStyle;
  buttonContainer: ViewStyle;
  loginButton: ViewStyle;
  container: ViewStyle;
}

const styleSheets: Styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  inputContainer: {
    borderColor: '#525252',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    width:250,
    height:45,
    marginBottom:20,
    flexDirection: 'row',
    alignItems:'center'
  },
  inputs: {
    height:45,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
  },
  inputIcon: {
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: "#525252",
  },
  loginText: {
    color: '#FFFFFF',
  },
  Text: {
    color: '#525252',
  }  
};

export const styles = StyleSheet.create<Styles>(styleSheets);
