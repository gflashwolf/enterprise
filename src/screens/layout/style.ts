import {
  StyleSheet, ViewStyle, FlexStyle, ImageStyle, TextStyle, Dimensions, Platform,
 } from 'react-native';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

interface Styles {
  drawerCover: ImageStyle;
  drawerImage: ImageStyle;
  badgeText: TextStyle;
  text: TextStyle;
}

const styleSheets: Styles = {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    position: "relative",
    marginBottom: 10
  },
  drawerImage: {
    position: "absolute",
    left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: "cover"
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 16,
    marginLeft: 20
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};

export const styles = StyleSheet.create<Styles>(styleSheets);
