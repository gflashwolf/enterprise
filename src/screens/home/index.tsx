import React, { Component } from "react";
import { styles } from "./style";
import { Container, ListItem, Thumbnail, Text, View, Item, Input, Icon, Button, Right, Left, Body } from 'native-base';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';
import { AppState } from '../../store';
import * as CompanyActions from '../../state/company/actions';
import { Company } from '../../state/company/types';
import { NavigationScreenProp } from 'react-navigation';
import Header from '../../components/Header';
import { baseURL } from '../../../src/state/utils/api';

interface StateProps {
  navigation: NavigationScreenProp<any, any>;
  enterprises: Company[];
}

interface DispatchProps {
  getCompanies(): void;
}

type Props = StateProps & DispatchProps;

interface State {
  shadowOffsetWidth: number;
  shadowRadius: number;
  currentSearch: string;
}

class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4,
      currentSearch: '',
    };
  }

  componentWillMount() {
    const { getCompanies } = this.props;
    getCompanies();
  }

  filterList = () => {
    const { enterprises } = this.props;
    const { currentSearch } = this.state;
    let filteredEnterprises: Company[];
    if ( currentSearch.length ) {
      filteredEnterprises = enterprises.filter((item: Company) => item.enterprise_name.toUpperCase().match(currentSearch.toUpperCase()))
    } else {
      filteredEnterprises = [...enterprises];
    }
    return filteredEnterprises;
  }
  
  renderSearch = ()=>{
    const { currentSearch } = this.state
    return (
      <View style={styles.ContentInput} >
        <Item>
          <Icon name='search' />
          <Input
            onChangeText={(text: string) =>  this.setState({ currentSearch: text })}
            value={currentSearch}
            placeholder='Pesquisar'/>
          <Icon type="FontAwesome" name="filter" />      
        </Item>
      </View>
    );
  }

  navigation = (item : Company)=>{
    this.props.navigation.navigate({
      routeName: "Companies",
      params: {
        enterprise: item
      }
    });
  }

  renderListItem = ( { item }: { item: Company } ) => (
    <ListItem thumbnail onPress={() => this.navigation(item)}>
      <Left>
        <Thumbnail square source={{uri:`${baseURL}${item.photo}`}} />
      </Left>
      <Body>
        <Text style={[styles.text,styles.margin]}>{item.enterprise_name} - $ {item.share_price}</Text>
        <Text style={styles.textSub} note numberOfLines={1}>{item.city} - {item.country}</Text>
      </Body>
      <Right>
        <Button onPress={() => this.navigation(item)} transparent>
          <Text style={styles.text}>Ver</Text>
        </Button>
      </Right>
    </ListItem>
  );

 

  bottonFloat = () => (
    <View style={styles.buttonFloat}>
      <Button transparent>
        <Icon style={styles.styleIcon} name="md-add"/>
      </Button>
    </View>
  );

  render() {
    return (
      <Container>
        {Header(this.props)}
        <View style={styles.Content}>
          {this.renderSearch()}
          <FlatList
            data={this.filterList()}
            horizontal={false}
            renderItem={(item)=>this.renderListItem(item)}
            keyExtractor={(item) => item.id.toString()}
          />
         {this.bottonFloat()}
        </View>
      </Container>
    );
  }
}
const mapStateToProps = (state: AppState) => ({
  enterprises: state.companies.data,
});

const mapDispatchToProps = (dispatch: Dispatch) => (bindActionCreators(CompanyActions, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Home);
