import {
  StyleSheet, ViewStyle, FlexStyle, ImageStyle, TextStyle, Dimensions, Platform,
 } from 'react-native';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

interface Styles {
  drawerCover: FlexStyle;
  drawerImage: ImageStyle;
  badgeText: TextStyle;
  textSub: TextStyle;
  styleIcon: TextStyle;
  margin: TextStyle;
  text: TextStyle;
  input: TextStyle;
  ContentInput: ViewStyle;
  Content: ViewStyle;
  buttonFloat: ViewStyle;
}

const styleSheets: Styles = {
  drawerCover: {
    alignSelf: "stretch",
    height: deviceHeight / 3.5,
    position: "relative",
    marginBottom: 10
  },
  textSub:{
    fontWeight:"300",
    fontSize: 11,
  },
  margin:{
    margin:3,
    fontSize: 18,
  },
  Content: {
    flex: 1,
    margin:3,
    backgroundColor: '#FFFFFF',
  },  
  ContentInput: {
    margin:4,
    padding:4,
  },  
  input:{
    borderWidth:2,
    borderRadius:30,
  },
  drawerImage: {
    position: "absolute",
    left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
    top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: "cover"
  },
  buttonFloat:{
    position: 'absolute',
    bottom: deviceHeight * 0.05,
    right: deviceWidth * 0.03,
    borderRadius: 25,
    width: 50,
    height: 50,
    alignContent: 'center',
    alignItems: 'center',
    zIndex: 10,
    elevation: 1,
    backgroundColor:'#5cb85c'
  },
  styleIcon:{
    color:'#FFFFFF',
    fontSize:30,
    marginTop:1,
    paddingTop:1,
    alignContent: 'center',
    alignSelf:'center'
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 12,
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined
  }
};

export const styles = StyleSheet.create<Styles>(styleSheets);
