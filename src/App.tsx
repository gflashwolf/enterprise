import React, { useRef } from 'react';
import { Provider } from 'react-redux';
import { SafeAreaView } from 'react-navigation';
import { StatusBar } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './store';
import AppNavigator from './routes';
import Messager from './components/Messager';
import LoadingContainer from './screens/LoadingContainer';
import NavigationService from './state/utils/navigation';

const App = () => {
  const Navigator = () => (
    <AppNavigator
      ref={
        (navigatorRef) => {
          if (navigatorRef) {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }
      }}
    />
  );

  return (
    <>
      <StatusBar translucent barStyle="dark-content" backgroundColor="transparent" />
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Navigator />
            <LoadingContainer />
            <Messager />
          </PersistGate>
        </Provider>
    </>
  );
};
export default App;
