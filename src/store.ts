/* global __DEV__ */
import {
  applyMiddleware, createStore, combineReducers, Store,
} from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import * as reducers from './state';
import { UserState } from './state/user/types';
import { CompanyState } from './state/company/types';
import { MessagesState } from './state/messages/types';
import rootSaga from './state/rootSaga';

const rootReducer = combineReducers(reducers);
const sagaMiddleware = createSagaMiddleware();
const enhancer = applyMiddleware(sagaMiddleware);

export interface AppState {
  user: UserState;
  messages: MessagesState;
  companies: CompanyState;
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['user', 'companies'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store: Store<AppState> = __DEV__
  ? createStore(persistedReducer, composeWithDevTools(enhancer))
  : createStore(persistedReducer, enhancer);

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export { store, persistor };
