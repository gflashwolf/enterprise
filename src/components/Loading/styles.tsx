import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    padding: 16,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  text: {
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontSize: 16,
    color: 'white',
    marginBottom: 16,
  },
});

export default styles;