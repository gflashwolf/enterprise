import React, { PureComponent } from 'react';
import {
  ActivityIndicator,
} from 'react-native';
import { Transitioning, Transition } from 'react-native-reanimated';
import styles from './styles';

interface Props {
  visibility: boolean;
  onFadeOut?: () => void;
  onMount?: () => void;
}

class Loading extends PureComponent<Props> {
  onPoseComplete = () => {
    if (!this.props.visibility && this.props.onFadeOut) {
      this.props.onFadeOut();
    }
  }

  render() {
    const transition = <Transition.Change interpolation="easeInOut" />;
    return (
      <>
        {
          this.props.visibility
          && (
          <Transitioning.View
            style={styles.container}
            transition={transition}
          >
            <ActivityIndicator size="large" color="#ffffff" />
          </Transitioning.View>
          )
        }
      </>
    );
  }
}

export default Loading;
