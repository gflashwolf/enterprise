import globalStyle from '../../../assets/style';

export default {
  customMessageContainer: {
    padding: 16,
    flexDirection: 'row',
    backgroundColor: globalStyle.colorSmoke,
    borderRadius: 10,
  },
  customTitle: { ...globalStyle.boxStore, color: globalStyle.colorPurple, marginTop: 30 },
  customBody: { ...globalStyle.text14, color: globalStyle.colorDark, marginTop: 12 },
};