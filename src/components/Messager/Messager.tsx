import React, { useEffect } from 'react';
import DropdownAlert from 'react-native-dropdownalert';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';
import { AppState } from '../../store';
import * as MessageActions from '../../state/messages/actions';
import styles from './styles';
import { Message } from '../../state/messages/types';

interface StateProps {
  messages: {
    title: string;
    body: string;
  }[];
  opened: boolean;
}

interface DispatchProps {
  shiftMessage(): void;
  setOpened(): void;
  addMessage(message: Message): void;
}

type Props = StateProps & DispatchProps;

const Messager: React.SFC<Props> = (props: Props) => {
  let dropDownAlertRef: DropdownAlert;

  const setOpen = () => {
    setTimeout(() => {
      props.setOpened();
    }, 1000);
  };

  const showMessage = () => {
    if (props.messages[0] && !props.opened) {
      const message = props.messages[0];
      dropDownAlertRef.alertWithType('custom', message.title, message.body);
      props.setOpened();
      props.shiftMessage();
    }
  };

  useEffect(() => {
    showMessage();
  });

  return (
    <>
      <DropdownAlert
        errorImageSrc=""
        successImageSrc=""
        closeInterval={3500}
        ref={(ref: DropdownAlert) => { dropDownAlertRef = ref; }}
        containerStyle={styles.customMessageContainer}
        titleStyle={styles.customTitle}
        messageStyle={styles.customBody}
        elevation={2}
        renderImage={() => null}
        updateStatusBar={false}
        onClose={() => setOpen()}
      />
    </>
  );
};

const mapStateToProps = (state: AppState) => state.messages;

const mapDispatchToProps = (dispatch: Dispatch) => (bindActionCreators(MessageActions, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(Messager);
