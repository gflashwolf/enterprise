import React from 'react';
import {  Platform } from 'react-native';
import {  Header, Icon, Button, Right, Left, Body, Title } from 'native-base';
import { NavigationScreenProp } from 'react-navigation';

interface StateProps {
  navigation: any;
}

const renderHeader = ({ navigation }: StateProps) => (
    <Header style={ Platform.OS === 'ios' ? { } : { marginTop: 24 } }>
        <Left>
          <Button onPress={() => navigation.openDrawer()} transparent>
            <Icon name='menu' />
          </Button>
        </Left>
        <Body>
          <Title>Home</Title>
        </Body>
        <Right/>
    </Header>
);

export default renderHeader;