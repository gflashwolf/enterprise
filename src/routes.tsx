import Home from "./screens/home";
import React from 'react';
import Login from "./screens/login";
import Companies from "./screens/companies";
import SideBar from "./screens/layout/Sidebar";
import { Platform } from 'react-native';
import {
  createDrawerNavigator,
  createAppContainer,
  createStackNavigator
} from 'react-navigation';

const Drawer = createAppContainer(createDrawerNavigator(
  {
    Home: { screen: Home },
    Companies: {screen: Companies}
  },
  {
    initialRouteName: "Home",
    contentOptions: {
      activeTintColor: "#e91e63",
      style: {
        marginTop: Platform.OS === 'ios' ? 0 : 24,
      },
    },
    contentComponent: ( props: any ) => <SideBar {...props} />
  }
));

const AppNavigator = createStackNavigator(
  {
    Drawer: {screen: Drawer},
    Login: { screen: Login }
  },
  {
    initialRouteName: "Login",
    headerMode: "none"
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer
