import { NavigationContainerComponent, NavigationActions } from 'react-navigation';

let navigator: NavigationContainerComponent;

function setTopLevelNavigator(navigatorRef: NavigationContainerComponent) {
  navigator = navigatorRef;
}

const navigation = () => navigator;
function navigate(routeName: string, params?: [any] | any) {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}

function back() {
  navigator.dispatch(
    NavigationActions.back(),
  );
}

export default {
  navigate,
  back,
  setTopLevelNavigator,
  navigation,
};
