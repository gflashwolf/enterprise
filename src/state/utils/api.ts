import axios, { AxiosInstance, AxiosResponse } from 'axios';

export const baseURL = 'https://empresas.ioasys.com.br/api/v1/';

const instance: AxiosInstance = axios.create({
    baseURL: baseURL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  // @ts-ignore
  instance.interceptors.response.use((response) => {
    if ( response.headers.client) {
      instance.defaults.headers.common['access-token'] = response.headers['access-token'];
      instance.defaults.headers.common['uid'] = response.headers.uid;
      instance.defaults.headers.common['client'] = response.headers.client;
    }
    return  Promise.resolve(response);
  }, (error) => {
    return Promise.reject(error)
  })

  
  export default instance as AxiosInstance;

