import { Company } from '../company/types';
export enum UserTypes {
  SIGN_IN_REQUEST = 'user/SIGN_IN_REQUEST',
  SIGN_IN_SUCCESS = 'user/SIGN_IN_SUCCESS',
  SIGN_IN_ERROR = 'user/SIGN_IN_ERROR',
}

export interface User {
  id: number;
  investor_name: string;
  email: string;
  city: string;
  country: string;
  balance: number;
  photo: string | boolean;
  portfolio: portfolio;
  portfolio_value: number;
  first_access: boolean;
  super_angel: boolean;
}

export interface portfolio {
  enterprises_number: number;
  enterprises: Company[];
}

export interface UserState { 
  data: User,
  loading: boolean,
}
