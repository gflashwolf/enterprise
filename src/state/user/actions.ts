/* eslint-disable max-len */
import { action } from 'typesafe-actions';
import {
  UserTypes, User,
} from './types';

export const signIn = (email: string, password: string) => action(UserTypes.SIGN_IN_REQUEST, { email, password });
export const signInSuccess = (user: User) => action(UserTypes.SIGN_IN_SUCCESS, { data: user });