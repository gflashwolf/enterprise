/* eslint-disable max-len */
import {
  all, call, put, takeLatest,
} from 'redux-saga/effects';
import { UserTypes, User } from './types';
import * as actions from './actions';
import * as repository from './repository';
import Navigation from '../../state/utils/navigation';
import { MessageTypes } from '../messages/types';

export function* signIn(action: any) {
  try {
    const { data } = yield call(repository.signIn, action.payload.email, action.payload.password);
    yield put(actions.signInSuccess(data.investor));
    yield call(Navigation.navigate, 'Home');
  } catch (error) {
    yield put({ type: UserTypes.SIGN_IN_ERROR });
    yield put({ type: MessageTypes.ADD_ERROR, payload: error });
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(UserTypes.SIGN_IN_REQUEST, signIn),
  ]);
}
