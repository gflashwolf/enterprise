import api from '../utils/api';

export const signIn = (email: string, password: string) => api.post('users/auth/sign_in',{email, password});
