import { Reducer } from 'redux';
import { UserState, UserTypes } from './types';

const INITIAL_STATE: UserState = {
  data: {
    id: 0,
    investor_name: '',
    email: '',
    city: '',
    country: '',
    balance: 0,
    photo: '',
    portfolio: {
      enterprises_number: 0,
      enterprises: [],
    },
    portfolio_value: 0,
    first_access: false,
    super_angel: false,
  },
  loading: false,
};

const reducer: Reducer<UserState> = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case UserTypes.SIGN_IN_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case UserTypes.SIGN_IN_SUCCESS: {
      return {
        ...state,
        data: payload.data,
        loading: false,
      };
    }
    case UserTypes.SIGN_IN_ERROR: {
      return {
        ...state,
        data: {},
        loading: false,
      };
    }
    default:
      return state;
  }
};

export default reducer;
