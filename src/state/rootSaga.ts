import { all } from 'redux-saga/effects';
import UserSagas from './user/services';
import CompanySagas from './company/services';


export default function* rootSaga() {
  return yield all([
    UserSagas(),
    CompanySagas(),
  ]);
}
