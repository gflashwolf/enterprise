export enum MessageTypes {
  ADD_ERROR = 'message/ADD_ERROR',
  ADD_SUCCESS = 'message/ADD_SUCCESS',
  ADD_MESSAGE = 'message/ADD_MESSAGE',
  SHIFT_MESSAGE = 'message/SHIFT_MESSAGE',
  SET_OPENED = 'message/SET_OPENED',
}

export interface Message {
  title: string;
  body: string;
}

export interface MessagesState {
  messages: Message[];
  opened: boolean;
}
