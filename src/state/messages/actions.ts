/* eslint-disable max-len */
import { action } from 'typesafe-actions';
import { MessageTypes, Message } from './types';

export const addError = (body: string) => action(MessageTypes.ADD_ERROR, body);
export const addSuccess = (body: string) => action(MessageTypes.ADD_SUCCESS, body);
export const addMessage = (message: Message) => action(MessageTypes.ADD_MESSAGE, message);
export const shiftMessage = () => action(MessageTypes.SHIFT_MESSAGE);
export const setOpened = () => action(MessageTypes.SET_OPENED);
