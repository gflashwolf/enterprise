import { Reducer } from 'redux';
import { MessagesState, MessageTypes } from './types';

const INITIAL_STATE: MessagesState = {
  messages: [],
  opened: false,
};

const reducer: Reducer<MessagesState> = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case MessageTypes.ADD_ERROR: {
      return {
        ...state,
        messages: state.messages.concat({
          title: 'Desculpe',
          body: payload,
        }),
      };
    }
    case MessageTypes.ADD_SUCCESS: {
      return {
        ...state,
        messages: state.messages.concat({
          title: 'Sucesso',
          body: payload,
        }),
      };
    }
    case MessageTypes.ADD_MESSAGE: {
      return {
        ...state,
        messages: state.messages.concat(payload),
      };
    }
    case MessageTypes.SHIFT_MESSAGE: {
      const list = [...state.messages];
      list.shift();
      return {
        ...state,
        messages: list,
      };
    }
    case MessageTypes.SET_OPENED: {
      return {
        ...state,
        opened: !state.opened,
      };
    }
    default:
      return state;
  }
};

export default reducer;
