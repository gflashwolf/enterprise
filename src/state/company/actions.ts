/* eslint-disable max-len */
import { action } from 'typesafe-actions';
import {
  CompanyTypes, Company,
} from './types';

export const getCompanies = () => action(CompanyTypes.GET_COMPANIES_REQUEST);
export const getCompaniesSuccess = (companies: Company[]) => action(CompanyTypes.GET_COMPANIES_SUCCESS, { companies });