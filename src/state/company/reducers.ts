import { Reducer } from 'redux';
import { CompanyState, CompanyTypes } from './types';

const INITIAL_STATE: CompanyState = {
  data: [],
  loading: false,
};

const reducer: Reducer<CompanyState> = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case CompanyTypes.GET_COMPANIES_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case CompanyTypes.GET_COMPANIES_SUCCESS: {
      return {
        ...state,
        data: payload.companies,
        loading: false,
      };
    }
    case CompanyTypes.GET_COMPANIES_ERROR: {
      return {
        ...state,
        data: {},
        loading: false,
      };
    }
    default:
      return state;
  }
};

export default reducer;
