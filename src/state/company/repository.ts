import api from '../utils/api';

export const getCompanies = () => api.get('enterprises');
