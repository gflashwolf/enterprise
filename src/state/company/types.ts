
export enum CompanyTypes {
    GET_COMPANIES_REQUEST = 'company/GET_COMPANIES_REQUEST',
    GET_COMPANIES_SUCCESS = 'company/GET_COMPANIES_SUCCESS',
    GET_COMPANIES_ERROR = 'company/GET_COMPANIES_ERROR',
  }
  
  export interface Company {
    id: number;
    enterprise_name: string;
    photo: string;
    share_price: number;
    city: string;
    country: string;
    description: string;
    enterprise_type: EnterpriseType;
  }
  export interface CompanyState { 
    data: Company[];
    loading: boolean;
  }
 export interface EnterpriseType {
  enterprise_type_name: string;
 }