/* eslint-disable max-len */
import {
  all, call, select, put, takeLatest,
} from 'redux-saga/effects';
import { CompanyTypes } from './types';
import * as actions from './actions';
import * as repository from './repository';
import { MessageTypes } from '../messages/types';

export function* getCompanies(action: any) {
  try {
    const { data } = yield call(repository.getCompanies);
    yield put(actions.getCompaniesSuccess(data.enterprises));
  } catch (error) {
    yield put({ type: CompanyTypes.GET_COMPANIES_ERROR });
    yield put({ type: MessageTypes.ADD_ERROR, payload: error });
  }
}

export default function* rootSaga() {
  yield all([
    takeLatest(CompanyTypes.GET_COMPANIES_REQUEST, getCompanies),
  ]);
}
