module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
  extends: [
    'airbnb',
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['react', 'import', 'jsx-a11y'],
  rules: {
    "react/prop-types":0,
    "eol-last": 0,
    "linebreak-style": 0,
    "no-plusplus": 0,
    "class-methods-use-this": 0,
    "react/jsx-one-expression-per-line": 0,
    'react/jsx-filename-extension': [
      'error',
      {
        extensions: ['.tsx'],
      },
    ],
    "camelcase": "off",
    "@typescript-eslint/camelcase": ["error", { "properties": "never" }],
    'import/prefer-default-export': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-member-accessibility': 'off',
    'max-len': [2, {"code": 120, "tabWidth": 4, "ignoreUrls": true}],
    'react/destructuring-assignment': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    "import/no-extraneous-dependencies": ["error", {"devDependencies": ['**/*.test.ts', '**/*.test.tsx']}],
    "import/no-unresolved": [ 2, { caseSensitive: false } ],
    "react/prefer-stateless-function": [0],
  },
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {},
      'node': {
        'paths': ['src'],
        'extensions': ['.js', '.jsx', '.json', '.native.js', '.ts', '.tsx'],
      },
    },
  },
}
